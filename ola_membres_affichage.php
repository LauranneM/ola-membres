<?php
class  ola_membres_affichage {



  public function __construct(){
    add_filter( 'manage_users_columns', array($this, 'ajoute_colonne'));
    add_filter( 'manage_users_custom_column', array($this, 'remplissage_tableau'), 10, 3 );
    add_filter( 'manage_users_sortable_columns', array($this, 'col_triables'));
    add_action( 'manage_users_extra_tablenav', array($this, 'select_form'));
  }

  public function ajoute_colonne( $colonne ) { // Ajoute une colonne pour les données de l'association au tableau des utilisateurs
    unset( $colonne['posts'] );
    if ( isset( $_REQUEST['ola_champ'] ) and $_REQUEST['ola_champ'] != 0 ) {
      $id_data = $_REQUEST['ola_champ'];
      global $wpdb;
      $requete = "SELECT description FROM {$wpdb->prefix}ola_champs WHERE id=".$id_data;
      $colonne['ola'] = $wpdb->get_row( $requete )->description;
    }
    else {
      $colonne['ola'] = 'Infos membres';
    }
    return $colonne;
  }

  public function remplissage_tableau( $output, $nom_colonne, $id ) { // Rempli les lignes du tableau
    if ( isset( $_REQUEST['ola_champ'] ) and $_REQUEST['ola_champ'] != 0 ) {
      $id_data = $_REQUEST['ola_champ'];
      global $wpdb;
      $requete = "SELECT libelle FROM {$wpdb->prefix}ola_champs WHERE id=".$id_data;
      $label = $wpdb->get_row( $requete )->libelle;
      $label_data = 'ola_'.$label;
    }
    else{
      $label_data = '';
      $id_data = 1;
    }
    switch ($nom_colonne) {
      case 'ola' :
        // var_dump($wpdb->get_row( $requete ));
        // var_dump(get_current_screen());
        return get_user_meta( $id, $label_data, $single = true);
        break;
    }
    return $output;
  }

  public function select_form(){ // Formulaire pour selectionner l'information à afficher dans la colonne personnalisée
    global $wpdb;
    $requete = "SELECT id, description FROM {$wpdb->prefix}ola_champs";
    $champs = $wpdb->get_results( $requete );
    if ( isset( $_REQUEST['ola_champ'] ) ){
      $select_id = $_REQUEST['ola_champ'];
    }

    $form = '
    <div class="action alignleft">
      <form class="action alignleft" action="" method="post">
        <select style="width:9em;" name="ola_champ">
        <option value="0">Sélectionner l\'info membre à afficher</option>
    ';
    foreach ($champs as $c){
      if ( $c->id == $select_id ){
        $selected = 'selected="selected"';
      }
      else {
        $selected = '';
      }
      $form .= '
        <option value='.$c->id.' '.$selected.'>'.$c->description.'</option>
      ';
    }
    $form .= '
        </select>
        <input type="submit" class="button button-secondary" value="Afficher">
      </form>
    </div>';
    echo $form;
    return $form;
  }

  public function col_triables($col){ // Ajoute les colonnes nom et ola à la liste des colonnes triables
    $col['ola'] = 'ola';
    $col['name'] = 'name';
    return $col;
  }

}
