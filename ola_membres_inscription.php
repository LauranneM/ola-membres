<?php
class  ola_membres_inscription {


  public function __construct(){
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_champ.php';
    add_action('admin_menu', array($this, 'add_admin_menu'), 20); //Création du sous-menu de création de $champs
    //On met une priorité de 20 pour s'assurer que le menu principal a bien été créé avant
    add_action('admin_init', array($this, 'register_settings'));
  }

  public function add_admin_menu() { // Crée le sous-menu Newsletter dans le panneau admin
      add_submenu_page(
        'ola-membres',                              //id_menu
        'Ola - Inscription membre', //admin_page_title
        'Inscription',                      //libellé du sous-menu
        'manage_options',                     // droits utilisateurs nécessaires pour voir cette page
        'ola_membres_inscription',                  // id sous menu
        array($this, 'menu_html')             // function d'affichage du contenu
        );
  }

  public function menu_html(){
    if (!current_user_can('manage_options')) {
        wp_die('Unauthorized user');
    }
    global $wpdb;
    // settings_errors( 'wporg_messages' );
    ?>
    <h1>Inscription d'un membre</h1>
    <p></p>
    <?php
    if (isset ($_REQUEST['ola_membres_inscription_email'])){
      $this->traitement();
    }
      echo '<form method="POST" action="">';
      settings_fields('ola_membres_inscription');
      do_settings_sections('ola_membres_inscription');
      submit_button('Inscrire');
      echo '</form>';
  }

  public function register_settings() { // Enregistre le groupe d'option pour le formulaire d'inscription'
    global $wpdb;
    $champs = $wpdb->get_results("SELECT libelle, type, description FROM {$wpdb->prefix}ola_champs");

    foreach ($champs as $c) {
      $libelle = 'ola_membres_inscription_'.$c->libelle;
      register_setting('ola_membres_inscription', $libelle);
    }
    add_settings_section('ola_membres_inscription_section', 'Renseignez les informations sur le membre :', array($this, 'section_html'), 'ola_membres_inscription');
    foreach ($champs as $c) {
      $champ = new ola_membres_champ();
      $libelle = 'ola_membres_inscription_'.$c->libelle;
      $champ->setLibelle($libelle);
      $champ->setType($c->type);
      add_settings_field($libelle, $c->description.' :', array($champ, 'formulaire_html'), 'ola_membres_inscription', 'ola_membres_inscription_section');
    }
  }
  public function section_html(){
    echo "<p>Les champs marqués d'un <span style='color:red;'>*</span> sont obligatoires</p>";

  }


  public function traitement() {  // Appelle ajout_membre si les champs obligatoires sont remplis
    echo ('<p>Le formulaire a été traité</p>');
    if (!empty($_REQUEST['ola_membres_inscription_nom']) && !empty($_REQUEST['ola_membres_inscription_prenom']) && !empty($_REQUEST['ola_membres_inscription_email'])) {
      $this->ajout_membre($_REQUEST['ola_membres_inscription_nom'], $_REQUEST['ola_membres_inscription_prenom'], $_REQUEST['ola_membres_inscription_email']);
    }
    else {
      echo ("<p style='color:red;'> Veuillez remplir les champs Nom, Prénom et E-mail de contact pour pouvoir inscrire un membre");
    }
  }
  public function ajout_membre($n, $p, $e) { // Ajoute le membre
    global $wpdb;
    $champs = $wpdb->get_results("SELECT libelle, description FROM {$wpdb->prefix}ola_champs;");
    // echo ("SELECT * FROM {$wpdb->prefix}ola_membres WHERE email='".$e."';");
    if ($wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_membres WHERE email='".$e."';")) {
      echo ("<p>L'e-mail ".$e." est déjà utilisé pour le membre ci-dessous. Vous pouvez modifier ce membre ou choisir une autre adresse e-mail");
    }
    else {
      $insertion ="
        INSERT INTO {$wpdb->prefix}ola_membres (nom, prenom, email)
        VALUES ('".$n."','".$p."','".$e."' );
      ";
      echo ($insertion);
      $wpdb->query($insertion);

      $id = $wpdb->get_results("SELECT id FROM {$wpdb->prefix}ola_membres WHERE email='".$e."';");
      foreach ($champs as $c) {
        $libelle = 'ola_membres_inscription_'.$c->libelle;
        if (isset($_REQUEST[$libelle]) and !empty($_REQUEST[$libelle])){
        $maj = "
          UPDATE {$wpdb->prefix}ola_membres
          SET ".$c->libelle." = '".$_REQUEST[$libelle]."'
          WHERE id='".$id."';
        ";
        $wpdb->query($maj);
          }
        }
        echo ("<p>Le membre a bien été ajouté avec les données suivantes :</p>");
    }
    $affichage = "<table>";
    $membre = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_membres WHERE email='".$e."';");
    foreach ($champs as $c) {
      $libelle = $c->libelle;
      $affichage .= "<tr><td>".$c->description."</td><td>".$membre->$libelle."</td></tr>";
      }
    $affichage .= "</table>";
    echo $affichage;
    // echo '<a href="http://localhost/wordpress/wp-admin/admin.php?page=ola_membres_inscription">
    //   Inscrire un autre membre</a>';
  }
}
