<?php
class  ola_membres_export {

  public function __construct(){
    add_action('admin_menu', array($this, 'add_admin_menu'), 20); //Création du
  }

  public function add_admin_menu() { // Crée le sous-menu Newsletter dans le panneau admin
      add_submenu_page(
        'ola-membres',                              //id_menu
        'Ola - Export des données membres', //admin_page_title
        'Export',                      //libellé du sous-menu
        'manage_options',                     // droits utilisateurs nécessaires pour voir cette page
        'ola_export',                  // id sous menu
        array($this, 'menu_html')             // function d'affichage du contenu
        );
  }

  public function menu_html(){  // affichage de la page du sous-menu
    if (!current_user_can('manage_options')) {  // vérification des droits utilisateur
        wp_die('Unauthorized user');
    }
    ?>
    <div class="wrap">
      <h1>Export Users</h1>
      <p>Cette page vous permet d'exporter les utilisateurs du site avec leurs informations. Toutes les informations seront exportées, y compris s'il reste des informations concernant des champs supprimés dans la base de données. Si vous ne souhaitez pas concerver ces informations, vous devez d'abord <a href=<?php echo admin_url('admin.php?page=ola_info_membres' );?>>nettoyer la base de données</a>.</p>
      <form class="" action="" method="post">
        <input type="submit" name="exporter" value="Exporter les utilisateurs">
      </form>
    <?php
    if ( isset( $_POST['exporter'] ) ){
      $this->export();
    }
    echo '
      <p>Si vous souhaitez télécharger d\'anciens fichiers d\'export, vous  pouvez les trouver <a href="';
    bloginfo('url');
    echo '/wp-content/uploads/">sur cette page</a>
      </div>
    ';
  }

  public function export(){
    global $wpdb;
    // Récupération des entêtes de colonne depuis les tables users et usermeta
    $sql_user = "SELECT * FROM {$wpdb->prefix}users";
    $sql_meta = "SELECT meta_key FROM {$wpdb->prefix}usermeta WHERE meta_key LIKE  \"ola\_%\" GROUP BY meta_key";
    // echo $sql_meta.'</br>';
    $users = $wpdb->get_results($sql_user);
    $meta_keys = $wpdb->get_results($sql_meta);
    // var_dump($meta_keys);
    $data = array();
    $headers = array('S.No','ID','Nom utilisateur','Email','Nom d\'affichage','Prénom','Nom','Date d\'inscription');
    foreach( $meta_keys as $meta_key ){
      $headers[] = $meta_key->meta_key;
    }
    // var_dump($headers);
    // Création du fichier csv avec les entêtes de colonnes
    $filename = get_bloginfo()."_export_membres_".time().".csv";
    $upload_dir = wp_upload_dir();
    $file = fopen($upload_dir['basedir'].'/'.$filename,"w");
    fputcsv($file, $headers);
    // Remplissage
    $sno = 1;
    foreach( $users as $user) {
      $user_meta = get_userdata($user->ID);
      $user_data=$sno."|".$user->ID."|".$user->user_login."|".$user->user_email."|".$user->display_name."|".$user_meta->first_name."|".$user_meta->last_name."|".$user->user_registered;
      foreach( $meta_keys as $meta_key ){
        $key = $meta_key->meta_key;
        if ( is_array( $user_meta->$key ) ){
          $user_data.="|".serialize( $user_meta->$key);
        }
        else {
          $user_data.="|".$user_meta->$key;
        }
      }
      $datas[] = $user_data;
      $sno++;
    }
    foreach( $datas as $data ){
      fputcsv( $file, explode('|', $data) );
    }
    fclose($file);

    $url = $upload_dir['baseurl'].'/'.$filename;
    $message = "Les informations des membres ont été exportée avec succès. <a href='".$url."' target='_blank'>Cliquez ici pour télécharger le fichier</a>";
    echo '<div class="updated">
      <p>'.$message.'</p>
      </div>';
  }



}
