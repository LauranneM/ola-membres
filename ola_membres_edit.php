<?php
class  ola_membres_edit {

  public function __construct(){
    // add the fields to user's own profile editing screen
    add_action('edit_user_profile', array($this, 'form_assoc'), 20);
    // add the fields to user profile editing screen
    add_action('show_user_profile',array($this, 'form_assoc'), 20);
    // add the save action to user's own profile editing screen update
    add_action('personal_options_update',array($this, 'assoc_data_update'), 20);
    // add the save action to user profile editing screen update
    add_action('edit_user_profile_update',array($this, 'assoc_data_update'), 20);
    add_action('user_new_form', array($this, 'form_assoc'), 20); // creating a new user
    add_action('user_register', array($this, 'assoc_data_update'), 20);
  }

  public function form_assoc($user) {
    echo ("<h2>Profil associatif de ".get_user_meta($user->ID, 'nickname', true)."</h2>
      <table class='form-table'>");
    global $wpdb;
    $champs = $wpdb->get_results("SELECT libelle, type, description, protege FROM {$wpdb->prefix}ola_champs");
    foreach ($champs as $c) {
      if ( !($c->protege) || current_user_can('edit_users') ){
        $metaKey = 'ola_'.$c->libelle;
        $type = '';
        if ($c->type == 'varchar(255)'){
          $type="text";
        }
        elseif ($c->type == 'date'){
          $type="date";
        }
        elseif ($c->type == 'float(11,2)'){
          $type="text";
        }
        echo ('
          <tr>
            <th>
              <label for ="'.$metaKey.'">'.$c->description.'</label>
            </th>
            <td>
              <input type="'.$type.'"
                     id="'.$metaKey.'"
                     name="'.$metaKey.'"
                     class="regular-text"
                     value="'.esc_attr(get_user_meta($user->ID, $metaKey, true)).'">
            </td>
          </tr>
        ');
      }
    }
    echo ('</table>');
    // echo get_user_meta($user->ID, 'ola_nom', true);
  }

  public function assoc_data_update($user_id){
    // vérifie que l'utilisateur a l'autorisation d'éditer $user_id
    if (!current_user_can('edit_user', $user_id)) {
        return false;
    }
    else {
      // crée / mets à jour les meta-informations pour $user_id
      global $wpdb;
      $champs = $wpdb->get_results("SELECT libelle FROM {$wpdb->prefix}ola_champs");
      foreach ($champs as $c) {
        $metaKey = 'ola_'.$c->libelle;
        update_user_meta(
          $user_id,
          $metaKey,
          $_REQUEST[$metaKey]
        );
      }
    }
  }

}
