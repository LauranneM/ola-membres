<?php

class ola_membres_groupes
{
  public function __construct(){
    include_once plugin_dir_path( __FILE__ ).'models/ola_groupe.php';
    add_action( 'admin_menu', array($this, 'add_admin_menu' ), 20 );

  }


  public function add_admin_menu(){ //crée un sous-menu d'administration
    add_submenu_page(
      'ola-membres',           //id_menu de ratthement
      'Ola - Groupes',         //admin_page_title
      'Groupes',               //libellé du sous-menu
      'manage_options',        // droits utilisateurs nécessaires pour voir cette page
      'ola_groupes',           // id sous menu
      array($this, 'menu_html')// function d'affichage du contenu
    );
  }

  public function menu_html(){    //Gère l'affichage de la page d'administration
    if (!current_user_can('manage_options')) { // Vérification des droits utilisateurs
        wp_die('Unauthorized user');
    }
    if ( isset( $_POST['edit_id'] ) and isset( $_POST['supprimer'] ) ){
      $this->supprimer_groupe( $_POST['edit_id'] );
    }
    if (isset ($_REQUEST['nom'])){
     $this->traitement();
    }
    echo '
    <h1>Gestion des groupes</h1>
    <p>Cette page vous permet de gérer les groupes : pour vos cours, missions, etc.</p>
    ';
    $this->form_ajout_html();
  }

  public function form_ajout_html(){
    $responsables = get_users(['role__in' => ['ola_bureau', 'administrator']]);

    $variable = 'Ceci est une variable';
    echo 'Formulaire';
    include 'views/ola_formulaire_groupe.php';
  }

  public function traitement(){
    echo 'Traitement';
    var_dump($_REQUEST);
    // $groupe = new Ola_groupe( null,'nom','desc','cat',1,'jour', NULL, NULL, 0);
    // echo ('Contenu de $groupe : ');
    // var_dump($groupe);
    // $groupe->create();
  }

  public function supprimer_groupe(){
    echo 'Suppression';
  }


}
