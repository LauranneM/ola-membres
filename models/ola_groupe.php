<?php
class Ola_groupe {

  public $id = '';
  public $nom = '';
  public $description = '';
  public $categorie = '';
  public $responsable = '';
  public $jour = '';
  public $horaire_debut = '';
  public $horaire_fin = '';
  public $tarif = '';

  function __construct($id, $nom, $description, $categorie, $responsable, $jour, $horaire_debut, $horaire_fin, $tarif) {
    $this->id           = $id;
    $this->nom          = $nom;
    $this->description  = $description;
    $this->categorie    = $categorie;
    $this->responsable  = $responsable;
    $this->jour         = $jour;
    $this->horaire_debut= $horaire_debut;
    $this->horaire_fin  = $horaire_fin;
    $this->tarif        = $tarif;
  }

  public function create(){
    global $wpdb;
    $insertion = "
      INSERT INTO {$wpdb->prefix}ola_groupes VALUES (
        NULL,
        '".$this->nom."',
        '".$this->description."',
        '".$this->categorie."',
        '".$this->responsable."',
        '".$this->jour."',
        '".$this->horaire_debut."',
        '".$this->horaire_fin."',
        '".$this->tarif."'
      );
    ";
    //horaire_debut & horaire_fin are set to 00:00:00 if they are NULL
    echo ("Requête d'insertion effectuée :");
    var_dump($insertion);
    $wpdb->query($insertion);
  }

}
