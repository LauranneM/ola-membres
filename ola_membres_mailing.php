<?php

class ola_membres_mailing
{
  public function __construct(){
    add_action( 'admin_menu', array($this, 'add_admin_menu' ), 20 );
    // add_action( 'admin_init', array($this, 'register_settings' ) );
    // Gestion d'ajax
    add_action( 'wp_ajax_get_membres_par_ID', array( $this, 'get_membres_par_ID' ) );
    // add_action( 'wp_ajax_nopriv_ola_membres', array( $this, 'handle_ajax' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'mes_scripts' ) );
  }

  // public function handle_ajax(){
  //   // if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'ola_membres' ) ){
  //   //   wp_send_json_error();
  //   // }
  //   // wp_send_json_success( array(
  //   //   'script_response' => 'AJAX Request Received',
  //   //   'nonce'           => wp_create_nonce( 'ola_membres' ),
  //   // ) );
  // }

  public function get_membres_par_ID(){ // renvoie un tableau json de membres à partir de $_POST['ids']
    $membres = array();
    if (isset($_POST['ids'])){
      $ids = $_POST['ids'];
      foreach ($ids as $id){
        array_push($membres, get_user_by('ID', $id));
      }
      wp_send_json_success( $membres );
    }
  }

  function mes_scripts(){ // importe les scripts js
    wp_enqueue_script( 'ola_membres_js', plugin_dir_url( __FILE__ ) .'js/ola_membres_mailing.js', array ( 'jquery' ), '1.0');
    // wp_localize_script( 'ola_membres_js', 'ola_destinataires', array(
    //   'ajax_url' => admin_url( 'admin-ajax.php' ),
    //   // 'nonce'    => wp_create_nonce( 'ola_membres' ),
    //   'valeur'   => 'test'
    // ) );
  }

  public function add_admin_menu(){ //crée un sous-menu d'administration
    add_submenu_page(
      'ola-membres',                       //id_menu de ratthement
      'Ola - Envoyer mail', //admin_page_title
      'Envoyer mail',               //libellé du sous-menu
      'manage_options',             // droits utilisateurs nécessaires pour voir cette page
      'ola_mail',                  // id sous menu
      array($this, 'menu_html')     // function d'affichage du contenu
    );
  }

  public function menu_html(){    //Gère l'affichage de la page d'administration
    if (!current_user_can('manage_options')) { // Vérification des droits utilisateurs
        wp_die('Unauthorized user');
    }
    $this->form_recherche_html();

    if ( isset( $_REQUEST['ola_membres_recherche_valeur'] ) ){
      $resultat = $this->traitement_recherche();
    }
    // Affichage du formulaire d'envoi de mail
    if ( isset( $_REQUEST['destinataires'] ) && isset( $_REQUEST['objet'] ) && isset( $_REQUEST['message'] ) ) {
      $this -> envoi_mail();
    }
    else {
      echo '
        <form method="POST" action="" class="mail_form">
          <table class=form-table>
            <tr>
              <th scope="row"> <label for="objet">Destinataires : </label></th>
              <td><textarea cols="50" id="destinataires" name="destinataires" value=""></textarea></td>
            </tr>
            <tr>
              <th scope="row"> <label for="objet">Objet :</label> </th>
              <td><input type="text" size="50" name="objet"></td>
            </tr>
            <tr>
              <th scope="row"><label for="message">Contenu du message : </label></th>
              <td>
              <!--<textarea name="message" rows="10" cols="50"></textarea>-->
              </td>
            </tr>
          </table>
        <div style="width:98%; max-width:80em;">
      ';
      wp_editor(
        '',
        'message',
        [
          'teeny'         => true,
          // 'editor_class'  => 'ola_message',
          // 'editor_css'    => '<style>.ola_message{ width: 150px; }</style>'
        ]);
      echo '</div></br>
          <input type="submit" value="Envoyer le message" class="button button-primary">
        </form>
      ';
    }
  }

  public function form_recherche_html(){
    global $wpdb;
    $champs = $wpdb->get_results("SELECT libelle, description, type FROM {$wpdb->prefix}ola_champs");
    echo '
      <h1>Contacter les membres</h1>
      <p>Vous pouvez utiliser le formulaire ci-dessous pour rechercher des membres pour les ajouter aux destinataires, ou vous pouvez saisir directement les adresses e-mail dans le champs destinataires.</p>
      <form method="POST" action="" class="rech_form">
        <table class="form-table">
          <tr>
            <th scope="row">
              <label for="ola_membres_recherche_champs">Sélectionnez un critère de recherche</label>
            </th>
            <td>
              <select name="ola_membres_recherche_champs" class="ola_champs">
                <option value="0">Sélectionner un critère de recherche</option>
    ';
    foreach ( $champs as $c ) {
      $libelle = 'ola_'.$c->libelle;
      echo '<option value='.$libelle.'>'.$c->description.'</option>';
    }
    echo '
            </select> (facultatif)
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="ola_membres_recherche_valeur">Entrez une valeur à rechercher</label>
          </th>
          <td>
            <input type="text" name="ola_membres_recherche_valeur">
          </td>
        </tr>
      <table>
      <input type="submit" value="Rechercher" class="button button-secondary">
    ';

    echo '</form>';
  } // affiche le formulaire de recherche de membres

  public function traitement_recherche(){ //renvoie les résultats de la recherche
    global $wpdb;
    if ( isset ( $_REQUEST['ola_membres_recherche_valeur'] ) ){
      if ( $_REQUEST['ola_membres_recherche_champs'] != 0 ) {
        echo '<p>Vous devez sélectionner un critère de recherche</p>';
      }
      else {
        $metaKey = $_REQUEST['ola_membres_recherche_champs'];
        $valeur = $_REQUEST['ola_membres_recherche_valeur'];
          $resultat = get_users( array(
            'meta_key'     => $metaKey,
            'meta_value'   => $valeur,
            'meta_compare' => 'LIKE',
          ));
          $resultat_unique = array();
          foreach ( $resultat as $user ){
            if ( ! in_array( $user, $resultat_unique ) ){
              array_push( $resultat_unique, $user);
            }
          }
          $this->tableau_resultat($resultat_unique);
      }
    }
  }

  public function tableau_resultat($resultat){ // affiche les utilisateurs sous forme d'un tableau avec des cases à cocher pour les sélectionner
    echo ('<form method="POST" action="" class="dest_form">');
    echo ('
      <h3>Sélectionnez les utilisateurs à ajouter à la liste des destinataires :</h3>
      <table class="wp-list-table widefat fixed striped users")>
      <thead>
      <tr>
        <th class="check-column"><input id="tous" type="checkbox" name="tous" value="tous"></th>
        <td>Tout sélectionner</td>
      </tr>
      </thead>
    ');
    foreach ( $resultat as $membre ) {
      $nom = '';
      if ( ! empty($membre->get( 'first_name' ) ) && ! empty( $membre->get( 'last_name' ) ) ) {
        $nom = $membre->get( 'first_name' ).' '.$membre->get( 'last_name' );
      }
      else {
        $nom = $membre->get( 'nickname' );
      }
      $id = $membre->get('ID');
      echo ('
      <tr>
        <th class="check-column"><input type="checkbox" name="membres[]" class="membres" value="'.$id.'" id="membre'.$id.'"></th>
        <td>'.$nom.'</td>
      </tr>');
    }
    echo '</table>';
    echo '</br><input type="submit" class="button button-primary" id="ajout_dest" value="Ajouter aux destinataires" >';
    echo '</form>';
  }

  public function envoi_mail(){ // Envoie le mail !
    // $resultat = array();
    $objet = $_REQUEST['objet'];
    $message = $_REQUEST['message'];
    $dest = $_REQUEST['destinataires'];
    $header = array();
    $header[] = 'From: '.wp_get_current_user()->display_name.'<'.wp_get_current_user()->user_email.'>';
    $header[] = 'Content-Type: text/html; charset=UTF-8';
    $resultat = wp_mail( $dest, $objet, $message, $header);
    $recapitulatif = '
      <p> Vous venez d\'envoyer le message suivant  via le site '.get_bloginfo( ).':</p>
      <table>
        <tr>
          <th scope="row">Destinataires :</th>
          <td> ' . $dest . '</td>
        </tr>
        <tr>
          <th scope="row">Object :</th>
          <td> ' . $objet . '</td>
        </tr>
        <tr>
          <th scope="row">Message : </th>
          <td> ' . $message . '</td>
        </tr>
      </table>';
    $header_conf[] = 'Content-Type: text/html; charset=UTF-8';
    $result_conf = wp_mail(wp_get_current_user()->user_email, $objet, $recapitulatif, $header_conf);
    if ( $result_conf ){
      echo 'Le message a bien été envoyé. Vous en recevrez une copie sur l\'adresse mail enregistrée dans votre profil';
    }
    else {
      echo '<p>Le message de confirmation n\'a pas pu être envoyé sur votre adress mail. Vérifiez l\'adresse mail enregistrée dans votre profil. Voici le récapitulatif du message envoyé : <p>'.$recapitulatif;
    }
    // foreach ( $resultat as $_dest => $_resultat ){
    if (!$resultat) {
      echo '<p> Un problème a été rencontré pour envoyer le mail à '.$dest.'.</p>';
    // }
    }


  } // envoie le mail


}
?>
