# Outils libres pour les associations

OLA membres est le premier d'une suite de plugin pour transformer un site worpress en suite complète pour les associations.
OLA vous permet de retrouver sur votre site associatif wordpress toutes les informations concernant vos membres : plus besoin d'outils externes pour suivre votre liste de membres.

## Fonctionnalités

+ Gestion des membres intégrée au site wordpress
+ Ajout d'informations sur les membres spécifiques aux besoin de l'association
+ Ajouts des rôles spécifiques aux associations, avec des permissions adaptées : membre de l'association, membre du bureau
+ Envoi de mails à des groupes de membres, avec possibilité de filtrer suivant n'importe quelle information renseignée.
+ Ajout de données "protégées", auxquelles seuls les membres du bureau ont accès (ex : cotisation payée)
+ Chaque membre peut accéder à son profil et modifier ses informations

## Installation du plugin

1. Télécharger le plugin en .zip
2. Dans le panneau d'administration de wordpress, allez sur la page d'ajout de plugin et choisissez "Upload", puis sélectionner le zip que vous venez de télécharger.
3. Le plugin est prêt à être utilisé et devrait apparaître dans le menu du panneau d'administration.