<?php
class  ola_membres_gestion_bdd {

  public function __construct(){
    add_action('admin_menu', array($this, 'add_admin_menu'), 20); //Création du sous-menu de création de $champs
    //On met une priorité de 20 pour s'assurer que le menu principal a bien été créé avant
  }

  public function add_admin_menu() { // Crée le sous-menu Newsletter dans le panneau admin
      add_submenu_page(
        'ola-membres',                              //id_menu
        'Ola - Informations membres', //admin_page_title
        'Infos membres',                      //libellé du sous-menu
        'manage_options',                     // droits utilisateurs nécessaires pour voir cette page
        'ola_info_membres',                  // id sous menu
        array($this, 'menu_html')             // function d'affichage du contenu
        );
  }

  public function menu_html(){  // affichage de la page du sous-menu
    if (!current_user_can('manage_options')) {  // vérification des droits utilisateur
        wp_die('Unauthorized user');
    }
    // Suppression du champ si un bouton supprimer a été cliqué
    if ( isset( $_POST['edit_id'] ) and isset( $_POST['supprimer'] ) ){
      $this->supprimer_champ( $_POST['edit_id'] );
    }
    if ( isset( $_REQUEST['nettoyer'])){
      $this->nettoyage_meta();
    }
    // Affichage du formulaire d'ajout de champ
    $this->form_ajout_html();
    // appel du traitement du formulaire d'ajout
    if (isset ($_REQUEST['description'])){
     $this->traitement();
    }
    // affichage des champs existants
    $this->tableau_html();
    echo '<p>Si vous souhaiter supprimer les données utilisateurs correspondant à des champs qui ont été supprimés ou qui ne sont plus utilisés, cliquer sur le bouton ci-dessous :</p>
      <form action="" method="POST">
      <input type=submit value="Nettoyer la base de données" name="nettoyer" class="button button-primary">
      </form>';
  }

  public function form_ajout_html(){
    global $wpdb;
    // initialisation des valeurs par défaut du formulaire
    $description = '';
    $type = [
      'texte'   => '',
      'date'    => '',
      'nombre'  => ''
    ];
    $protege = '';
    $bouton = 'valid_nouveau';
    $id = 0;
    $alert_edit = '';

    // modification des valeur par défaut si on souhaite éditer un champ
    if ( isset( $_REQUEST['editer'] ) and ( isset( $_REQUEST['edit_id'] ) ) and ( ! empty( $_REQUEST['edit_id'] ) ) ){
      $champ = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_champs WHERE id=".$_REQUEST['edit_id']);
      $description = $champ->description;
      $id = $champ->id;
      if ($champ->protege == 1){
        $protege = 'checked="checked"';
      }
      switch ($champ->type) {
        case "varchar(255)": $type['texte'] = 'selected="selected"';
        break;
        case "date": $type['date'] = 'selected="selected"';
        break;
        case "float(11,2)": $type['nombre'] = 'selected="selected"';
        break;
      }
      $bouton = 'valid_edit';
      $alert_edit = '<p>Si vous modifiez la description, les données de l\'ancien champ seront copiées dans le nouveau champ. Pour supprimer les données de l\'ancien champ, utilisez le bouton de nettoyage de la base de données</p>';
    }

    // affichage du formulaire
    echo ('
      <h1>Gestion des informations des membres</h1>
      <p>Sur cette page, vous pouvez gérer les informations que vous souhaiter enregistrer sur chaque membre dans la base de données</p>
      <h3>Ajouter un champ</h3>
      <form method="POST" action="">
        <table class="form-table">
        <input type="hidden" name="id" value="'.$id.'">
          <tr>
            <th scope="row"> <label for="description">Description du champ :</label></th>
            <td>  <input type="text" class="regular-text" name="description" value="'.$description.'" maxlength="64">'.$alert_edit.'</td>
          </tr>
          <tr>
            <th scope="row"> <label for="type">Type :</label></th>
            <td>
                <select name="type">
                  <option value="varchar(255)" '.$type['texte'].'>Texte</option>
                  <option value="date" '.$type['date'].'>Date</option>
                  <option value="float(11,2)" '.$type['nombre'].'>Nombre</option>
                </select>
                <p>Attention ! N\'utiliser le type nombre que pour des vrais nombres, sur lesquels vous voudriez faire des calculs (ex : montant cotisation, âge). Utiliser le type "texte" pour les champs représentant un code (ex : numéro de téléphone, code postal).</p>
            </td>
          </tr>
          <tr>
            <th scope="row"> Champ protégé</th>
            <td>
              <input type="checkbox" name="protege" value="1" '.$protege.'><label for="protege">Ce champ est protégé</label></th>
              <p>Les champs protégés ne peuvent être vus et modifiés que par les utilisateurs ayant des droits d\'admistration du site. Vous pouvez utiliser cette option pour des champs indiquant par exemple si le membre a payé sa cotisation, ou s\'il est adhérant pour l\'année en cours.</p>
            </td>
          </tr>
        </table>
        <input type="submit" name="'.$bouton.'" value="Enregistrer" class="button button-primary">
      </form>
    ');
  }


  public function traitement() {  // Appelle ajout_champ($description) si ola_membres_champs_description existe
    if (!empty($_REQUEST['description'])) {
      $description = $_REQUEST['description'];
      $this->ajout_champ($description);
    }
  }

  public function ajout_champ($d) { // Ajoute le champ $d
    global $wpdb;
    $label = ola_membres::labelize($d);
    if (isset($_REQUEST['id']) and !empty($_REQUEST['id'])){
      $id = $_REQUEST['id'];
    }
    if (isset($_REQUEST['type']) and !empty($_REQUEST['type'])){
      $type = $_REQUEST['type'];
    }
    else {
      $type = 'varchar(255)';
    }
    if (isset($_REQUEST['protege']) && !empty($_REQUEST['protege'])){
      $protege = $_REQUEST['protege'];
    }
    else {
      $protege = 0;
    }
    if ($id != 0 ) {
      $ancien_champ = $wpdb -> get_row ("SELECT libelle FROM {$wpdb->prefix}ola_champs WHERE id=".$id);
      // var_dump($ancien_champ->libelle);
      $insertion = "
        UPDATE {$wpdb->prefix}ola_champs
        SET
          libelle ='".$label."',
          description ='".$d."',
          type ='".$type."',
          protege ='".$protege."'
        WHERE id='".$id."';
      ";
      // echo $insertion;
      $wpdb->query($insertion);
      $this->maj_meta($id, $ancien_champ->libelle);
    }
    elseif ($id ==0 and $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}ola_champs WHERE libelle=".$label) ) {
      echo '<p>Le champ '.$d.' existe déjà, veuillez réessayer en choisissant un autre nom<p>';

    }
    else {
      $insertion ="
        INSERT INTO {$wpdb->prefix}ola_champs VALUES
          (NULL, '".$label."', '".$d."', '".$type."', '".$protege."');
      ";
      // echo $insertion;
      $wpdb->query($insertion);
    }

  }

  public function tableau_html(){
    echo '
    <h3>Champs existants</h3>
      <table class="wp-list-table widefat fixed striped">
        <tr>
          <th scope="col">Informations membres</th>
          <th scope="col">Type</th>
          <th scope="col">Protégé</th>
          <th scope="col">Action</th>
        </tr>
    ';
    global $wpdb;
    $champs = $wpdb->get_results("SELECT id, description, type, protege FROM {$wpdb->prefix}ola_champs");
    foreach ($champs as $_champ) {
      switch ($_champ->type) {
        case 'varchar(255)':
          $type = 'Texte';
          break;
        case 'date':
          $type = 'Date';
          break;
        case 'float(11,2)':
          $type = 'Nombre';
          break;
      }
      if ($_champ->protege == 1){
        $protege = 'oui';
      }
      else {
        $protege = 'non';
      }
      echo '
        <tr>
          <td>'.$_champ->description.'</td>
          <td>'.$type.'</td>
          <td>'.$protege.'</td>
          <td>
            <form method="POST" action="">
            <input type="hidden" name="edit_id" value="'.$_champ->id.'">
            <input type="submit" class="button-link" name="editer" value="Editer">
            <input type="submit" class="button-link suppr_bouton" name="supprimer" value="Supprimer">
            </form>
          </td>
        </tr>
      ';
    }
    // Le champ caché "edit_id" transmet l'id de l'élément à traiter.
    // Les boutons éditer et supprimer transmettent l'action à réaliser, suivant celui qui est cliqué.
    echo '
        </table>
    ';
  }

  public function maj_meta($id, $ancien) {
    global $wpdb;
    $data = $wpdb->get_results( "SELECT user_id, meta_value FROM {$wpdb->prefix}usermeta WHERE meta_key='ola_".$ancien."';" );
    // echo "SELECT user_id, meta_value FROM {$wpdb->prefix}usermeta WHERE meta_key='ola_".$ancien."';"  ;
    // var_dump($data);
    $nouvelle_cle = 'ola_'.$wpdb->get_row( "SELECT libelle FROM {$wpdb->prefix}ola_champs WHERE id=".$id )->libelle;
    // var_dump($nouvelle_cle);
    foreach ( $data as $_data ) {
      echo 'id = '.$_data->user_id.' meta_key :  '.$nouvelle_cle.' meta_value : '.$_data->meta_value;
      update_user_meta( $_data->user_id, $nouvelle_cle, $_data->meta_value );
    }
    // rechercher les utilisateurs avec la clé meta
    // pour chaque utilisateur, copier l'ancienne meta dans la nouvelle Clé.
  }

  public function supprimer_champ($id){
    global $wpdb;
    $requete = "SELECT * FROM {$wpdb->prefix}ola_champs WHERE id=".$id;
    $champ = $wpdb->get_row( $requete );
    switch ($champ->type) {
      case ('varchar(255)'):
        $type = 'Texte';
      break;
      case ('date'):
        $type = 'Date';
      break;
      case ('float(11,2)'):
        $type = 'Nombre';
      break;
    }
    if ($champ->protege){
      $protege = "Oui";
    }
    else {
      $protege = "Non";
    }
    $result = $wpdb->query("DELETE FROM {$wpdb->prefix}ola_champs WHERE id=".$id);
    if ( $result ){
      echo '
        <div id="message" class="updated notice is-dismissible">
          <p>Le champ <strong>'.$champ->description.'</strong> a été supprimé</br>
            Pour supprimer les données qui y étaient associées, cliquez sur le bouton <strong>Nettoyer la base de données</strong> en bas de cette page.</br>
            Si vous souhaiter annuler la suppression, vous pouvez simplement le recréer avec les informations suivantes:</p>
          <table>
            <tr>
              <th scope="row">Description : </th>
              <td>'.$champ->description.'</td>
            </tr>
            <tr>
              <th scope="row">Type : </th>
              <td>'.$type.'</td>
            </tr>
            <tr>
              <th scope="row">Champ protégé :    </th>
              <td>'.$protege.'</td>
            </tr>
          </table>
        </div>
      ';
    }

  }

  public function nettoyage_meta(){
    global $wpdb;
    $requete = "SELECT DISTINCT meta_key FROM {$wpdb->prefix}usermeta WHERE meta_key LIKE \"ola\_%\"" ;
    $data = $wpdb->get_results( $requete );
    $champs = $wpdb->get_results( "SELECT libelle FROM {$wpdb->prefix}ola_champs" );
    if ($champs) {
      foreach ( $champs as $c ) {
        $libelles[] = 'ola_'.$c->libelle;
      }
      echo '<div id="message" class="updated notice is-dismissible">';
      foreach ( $data as $_data ) {
        if ( ! in_array( $_data->meta_key, $libelles ) ) {
          $result = $wpdb->query("DELETE FROM {$wpdb->prefix}usermeta WHERE meta_key='".$_data->meta_key."';");
          if ($result) {
            echo '<p>Les données concernant le champ inutilisé <strong>'.$_data->meta_key.' </strong> ont été supprimées.</p>';
          }
          else {
            echo ("DELETE FROM {$wpdb->prefix}usermeta WHERE meta_key='".$_data->meta_key."'");
            echo '<p>Echec de la suppression des données pour le champ inutilisé <strong>'.$_data->meta_key.'</strong></p>';        }
        }
      }
      echo '</div>';
    }
    //rechercher les meta clé qui commencent par le slug ola
    // pour chaque meta clé, si elle n'est pas dans la liste des champs, supprimer les lignes dans la base de données.
  }

}
