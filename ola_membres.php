<?php

/*
Plugin Name: Ola - gestion des membres
Plugin URI: http://ola.laurannemarcel.fr
Description: Un plugin de gestion des membres pour les associations
Version: 1
Author: Lauranne Marcel
Author URI: http://ola.laurannemarcel.fr
License: GPL2
*/

class ola_membres
{
  public function __construct()
  {
    register_activation_hook(__FILE__, array($this, 'install'));
    register_uninstall_hook(__FILE__, array($this, 'uninstall'));
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_gestion_bdd.php';
    new ola_membres_gestion_bdd();
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_mailing.php';
    new ola_membres_mailing();
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_edit.php';
    new ola_membres_edit();
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_affichage.php';
    new ola_membres_affichage();
    include_once plugin_dir_path( __FILE__ ).'/ola_membres_export.php';
    new ola_membres_export();
    // include_once plugin_dir_path( __FILE__ ).'/ola_membres_groupes.php';
    // new ola_membres_groupes();

    add_action('admin_menu', array($this, 'add_admin_menu'));

  }
  public function add_admin_menu() { //Création du menu principal dans la partie admin pour la gestion du plugin
    add_menu_page(
      'Ola - Gestion des membres',  //admin_page_title
      'Ola - Gestion des membres',                        //libellé du menu
      'manage_options',                     // droits utilisateurs nécessaires pour voir cette page
      'ola-membres',                               // id_menu
      array($this, 'menu_html')             // function d'affichage du contenu
    );
  }

  public function install(){ // crée la table de données à l'activation du plugin et ajoute le rôle "Membre de l'association"
    global $wpdb;
    $wpdb->query("
      CREATE TABLE IF NOT EXISTS {$wpdb->prefix}ola_champs
        (id INT AUTO_INCREMENT PRIMARY KEY,
        libelle VARCHAR(255) NOT NULL,
        description VARCHAR(255),
        type VARCHAR(255),
        protege INT(1)
      );
    ");
    if ( ! ( $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}ola_champs WHERE libelle='adresse'" ) ) ) {
      $wpdb->query("
        INSERT INTO {$wpdb->prefix}ola_champs VALUES
          (NULL, 'adresse', 'Adresse postale', 'varchar(255)', 0);
      ");
    }
    if (!($wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_champs WHERE libelle='complement_adresse'"))){
      $wpdb->query("
        INSERT INTO {$wpdb->prefix}ola_champs VALUES
          (NULL, 'complement_adresse', 'Complément d\'adresse', 'varchar(255)', 0);
      ");
    }
    if (!($wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_champs WHERE libelle='code_postal'"))){
      $wpdb->query("
        INSERT INTO {$wpdb->prefix}ola_champs VALUES
          (NULL, 'code_postal', 'Code postal', 'varchar(255)', 0);
      ");
    }
    if (!($wpdb->get_row("SELECT * FROM {$wpdb->prefix}ola_champs WHERE libelle='ville'"))){
      $wpdb->query("
        INSERT INTO {$wpdb->prefix}ola_champs VALUES
          (NULL, 'ville', 'Ville', 'varchar(255)', 0);
      ");
    }
    $wpdb->query("
      CREATE TABLE IF NOT EXISTS {$wpdb->prefix}ola_groupes
        (id INT AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255) NOT NULL,
        description TEXT,
        catégorie VARCHAR(255),
        responsable BIGINT(20),
        jour VARCHAR(255),
        horaire_debut TIME(0),
        horaire_fin TIME(0),
        tarif FLOAT(11,2)
      );
    ");
    // remove_role( ('ola_membre') );
    add_role('ola_membre',
      'Membre de l\'association',
      array(
        'read'                => true,
        'read_private_posts'  => true,
        'read_private_pages'  => true
      )
    );
    add_role('ola_bureau',
      'Membre du bureau',
      array(
        // 'activate_plugins'  => true,
        'delete_others_pages'  => true,
        'delete_others_posts'  => true,
        'delete_pages'  => true,
        'delete_posts'  => true,
        'delete_private_pages'  => true,
        'delete_private_posts'  => true,
        'delete_published_pages'  => true,
        'delete_published_posts'  => true,
        'edit_dashboard'  => true,
        'edit_others_pages'  => true,
        'edit_others_posts'  => true,
        'edit_pages'  => true,
        'edit_posts'  => true,
        'edit_private_pages'  => true,
        'edit_private_posts'  => true,
        'edit_published_pages'  => true,
        'edit_published_posts'  => true,
        // 'edit_theme_options'  => true,
        'export'  => true,
        // 'import'  => true,
        'list_users'  => true,
        'manage_categories'  => true,
        'manage_links'  => true,
        'manage_options'  => true,
        'moderate_comments'  => true,
        'promote_users'  => true,
        'publish_pages'  => true,
        'publish_posts'  => true,
        'read_private_pages'  => true,
        'read_private_posts'  => true,
        'read'  => true,
        'remove_users'  => true,
        // 'switch_themes'  => true,
        'upload_files'  => true,
        // 'customize'  => true,
        // 'delete_site'  => true,
        // 'update_core'  => true,
        // 'update_themes'  => true,
        // 'update_plugins'  => true,
        // 'install_plugins'  => true,
        // 'install_themes'  => true,
        // 'upload_plugins'  => true,
        // 'upload_themes'  => true,
        // 'delete_themes'  => true,
        // 'delete_plugins'  => true,
        // 'edit_themes'  => true,
        // 'edit_plugins'  => true,
        // 'edit_files'  => true,
        'edit_users'  => true,
        'create_users'  => true,
        'delete_users'  => true,
        'unfiltered_html'  => true
      )
    );
  }

  public static function uninstall() { //Supprimme la table des champs lors de la désintallation
      global $wpdb;
      $wpdb->query("
        DROP TABLE IF EXISTS {$wpdb->prefix}ola_champs;
      ");
  }

  public function menu_html() { // crée le contenu de la page d'administration principale
      // Mode d'emploi du plugin_dir_path
      require (plugin_dir_path( __FILE__ ).'views/ola_membres_help.php');
  }

  public function labelize ($s) {
    $r = strtolower($s);
    $r = ltrim($r, "0123456789+-*()[]{}'#*%!:;,?./|§%€£_=$");
    $r = chop($r);
    $interdit = array(' ','+','=','(',')','[',']','{','}','#',',','?',';','.',':','/','!','§','*','%','@','|');
    foreach ($interdit as $caractere) {
      $r = str_replace($caractere, '_', $r);
    }
    $r = str_replace("ù", "u", $r);
    $r = str_replace("é", "e", $r);
    $r = str_replace("è", "e", $r);
    $r = str_replace("à", "a", $r);
    $r = str_replace("ç", "c", $r);
    $r = str_replace("€", "E", $r);
    return $r;
  }

}
new ola_membres();
