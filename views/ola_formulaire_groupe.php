<h3>Ajouter un groupe<h3>
<p><?php echo($variable) ?></p>
<form method="POST" action="">
  <table class="form-table">
    <input type="hidden" name="id" value="NULL">
    <tr>
      <th scope="row"><label for="nom">Nom du groupe :</label></th>
      <td><input type="text" class="regular-text" name="nom"></td>
    </tr>
    <tr>
      <th scope="row"><label for="description">Description :</label></th>
      <td><input type="text" class="regular-text" name="description"></td>
    </tr>
    <tr>
      <!--TODO: Créer une table des catégories pour faire un menu déroulant-->
      <th scope="row"><label for="categorie">Catégorie</label></th>
      <td><input type="text" class="regular-text" name="categorie"></td>
    </tr>
    <tr>
      <th scope="row"> <label for="responsable">Responsable</label> </th>
      <th><select class="regular-select" name="responsable">
        <option value="0" selected="selected">Choisir</option>
        <?php
          foreach ($responsables as $responsable) {
            echo ('<option value="'.$responsable->id.'">'.$responsable->display_name.'</option>');
          }
        ?>
      </select>
      <p>Les responsables de groupe doivent être membre du bureau, administrateur ou encadrant</th>
    </tr>
  </table>
  <input type="submit" name="enregistrer" value="Enregistrer" class="button button-primary">
</form>
