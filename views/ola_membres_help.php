<h1> <?php echo get_admin_page_title() ?></h1>
<p>Bienvenue sur la page d\'accueil du plugin</p>
<p>
  Le plugin "ola Membres" vous permet de gérer les membres de votre association sur le site internet de l'association
</p>
<h2>Configurer votre base de données</h2>
<p>
  La première chose à faire est de configurer la base de données. Vous devez choisir quelles données vous souhaitez enregistrer pour chaque membre (par exemple la date de naissance, l'instrument joué, le rôle dans l'associaition, etc.). Tout cela se fait depuis le menu <a href="<?php echo admin_url('admin.php?page=ola_info_membres') ?>">Informations membres</a>
</p>
<p>
  Certains champs sont déjà défnis par défaut dans le plugin pour vous donner un exemple.
</p>
<p>
  Vous pouvez également supprimer ou modifier des champs. Attention ! Si vous supprimez un champs cela supprimera les données enregistrées pour ce champs.
</p>
<h2>Inscrire un membre dans l'association</h2>
<p>
  Vous pouvez directement inscrire un membre grâce au menu <a href="<?php echo admin_url('user-new.php') ?>">Utilisateurs > Ajouter</a> du menu d'administration Worpress. Quand vous inscrivez un membre, n'oubliez pas de vérifier que la case "Envoyer un message au nouvel utilisateur à propos de son compte." est bien cochée, pour que le membre puisse recevoir par e-mail ses données de connection au site.
</p>
<p>
  Les membres peuvent également s'inscrire eux-même sur le site et saisir les informations liées à l'association. Pour cela, vous devez activer le widget "Meta" pour que les utilisateurs aient accès au lien d'inscription.
</p>
<h2>Rôles des membres</h2>
<p>
  Le plugin ola a créé 2 rôles spécifiques pour les associations : <strong>membre de l'association</strong> et <strong>membre du bureau</strong>. Ces rôles vous permettent d'attribuer des permissions adaptées aux personnes inscrites sur le site. Voici l'ensemble des rôles wordpress et spécifiques au plugins et leurs particularités :
</p>
  <table>
    <tr>
      <td>Abonné</td>
      <td>Toute personne inscrite sur le site. Chaque personnes enregistrée sur le site peut modifier son propre profil</td>
    </tr>
    <tr>
      <td><strong>Membre de l'association</strong> </td>
      <td>Peut lire les articles et pages privés</td>
    </tr>
    <tr>
      <td>Contributeur</td>
      <td>Peut écrire et gérer ses propres articles, mais ne peut pas les publier</td>
    </tr>
    <tr>
      <td>Auteur</td>
      <td>Peut publier et gérer ses propres articles</td>
    </tr>
    <tr>
      <td>Editeur</td>
      <td>Peut publier et gérer des articles et pages, y compris ceux écris par d'autres</td>
    </tr>
    <tr>
      <td><strong>Membre du bureau</strong> </td>
      <td>A les même autorisations qu'un éditeur, mais il peut en plus gérer les utilisateurs et faire des modifications légères sur le site</td>
    </tr>
    <tr>
      <td>Administrateur</td>
      <td>Peut tout gérer sur le site</td>
    </tr>
</table>
<h2>Modifier les informations des membres</h2>
<p>
  La modification des informations des membre peut se faire soit directement par le membre soit par un administrateur du site par l'intermédiaire du menu utilisateur (dans les menus de gauche du panneau d'administration).
</p>
<h2>Export des données</h2>
<p>
  Vous pouvez exporter les données des utilisateurs du site depuis le menu <a href="<?php echo admin_url('admin.php?page=ola_export') ?>">Export</a>.
</p>
