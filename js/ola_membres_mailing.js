window.ola_membres_ajax = ( function( window, document, $ ){
  var app = {};

  app.cache = function(){
    app.$ajax_form = $( '.dest_form' );
  };

  app.init = function(){
      app.cache();
      app.$ajax_form.on( 'submit', app.form_handler );
      // app.affiche([]);
  };

  app.form_handler = function( evt ){
    evt.preventDefault();
    let dest = new Array();
    $.each($("input[name='membres[]']:checked"), function() {
      dest.push($(this).val());
    });
    app.post_ajax( dest );
  };

  app.post_ajax = function( data ){
    var post_data = {
      'action': 'get_membres_par_ID',
      'ids' : data,
      // nonce      : ola_membres.nonce,
    };
    $.post( ajaxurl, post_data, app.ajax_response, 'json' )
  };

  app.ajax_response = function(response_data){
    let dest = [];
    if( response_data.success ){
      // ola_membres.nonce = response_data.data.nonce;
      for (let user of response_data.data){
        dest[user.ID]={
          nom:    user.data.display_name,
          email:  user.data.user_email,
        }
      }
      app.affiche(dest);
    }
  };

  app.affiche = function(dest){ // ajout les adresse des destinataires dans le champ approprié
    let contenu =$('#destinataires').val();
    if (dest.length > 0) {
      for (let i = 0; i < dest.length; i++) {
        if (dest[i] != undefined) {
          let new_dest = dest[i].nom + ' <' + dest[i].email + '>';
          if (contenu.search(new_dest) == -1){ //ajoute le destinataire au champ si il n'y est pas déjà
            if (contenu != ''){
              contenu += ', '
            }
            contenu += new_dest;
          }
        }
      }
    }
  $('#destinataires').val(contenu);
  }

  $(document).ready( app.init );

  return app;

})( window, document, jQuery );
